#!/usr/bin/python3

"""
Copyright 2017 Lukas Toggenburger; https://github.com/ltog/mapillary-osm-usage-notification-bot

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import mounb
import pytest

def test_test():
    assert 1 == 1

def test_get_mapillary_key_matches():
    # VALID MAPILLARY KEYS

    # most simple case
    assert mounb.get_mapillary_key_matches("1234567890123456789012") == ["1234567890123456789012"]

    # invalid characters around mapillary keys are skipped
    assert mounb.get_mapillary_key_matches("%1234567890123456789012&") == ["1234567890123456789012"]

    # standard case for multiple mapillary keys
    assert mounb.get_mapillary_key_matches("1234567890123456789012; abcdefghijklmnopqrstuv") == ["1234567890123456789012", "abcdefghijklmnopqrstuv"]

    # variant of standard case for multiple mapillary keys
    assert mounb.get_mapillary_key_matches("1234567890123456789012;abcdefghijklmnopqrstuv") == ["1234567890123456789012", "abcdefghijklmnopqrstuv"]

    # space only is probably rare but recognized anyway
    assert mounb.get_mapillary_key_matches("1234567890123456789012 abcdefghijklmnopqrstuv") == ["1234567890123456789012", "abcdefghijklmnopqrstuv"]

    # keys are recognized even when there is no spacing character in between
    assert mounb.get_mapillary_key_matches("1234567890123456789012abcdefghijklmnopqrstuv") == ["1234567890123456789012", "abcdefghijklmnopqrstuv"]

    # comprehensive test
    assert mounb.get_mapillary_key_matches(".%&;1234567890123456789012;abcdefghijklmnopqrstuv; ABCDEFGHIJKLMNOPQRSTUV%") == ["1234567890123456789012", "abcdefghijklmnopqrstuv", "ABCDEFGHIJKLMNOPQRSTUV"]


    # INVALID MAPILLARY KEYS

    # no content at all
    assert mounb.get_mapillary_key_matches("") == []

    # only invalid characters
    assert mounb.get_mapillary_key_matches(r"./\..%.$.&.[]$()?{}...") == []

    # wrong datatype
    with pytest.raises(TypeError) as e_info:
        mounb.get_mapillary_key_matches(None)
    assert str(e_info.type) == "<type 'exceptions.TypeError'>" # probably not needed since implicitly noted as argument of pytest.raises()
    assert str(e_info.value) == "expected string or buffer"

    # no argument
    with pytest.raises(TypeError) as e_info:
        mounb.get_mapillary_key_matches()
    assert e_info.match(r"takes exactly 1 argument \(0 given\)")
