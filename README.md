# mapillary-osm-usage-notification-bot

Notify Mapillary contributors when their images have been referenced in OpenStreetMap

## Current status

Still in development, the bot will post only sporadically.

## What is this about?

This is an automated software ("bot") that periodically checks additions to the [OpenStreetMap database](https://wiki.openstreetmap.org). Whenever it is detected that a [Mapillary](https://www.mapillary.com) image was referenced in OpenStreetMap, the referenced Mapillary image receives a comment. This comment states the mention together with a link to the OSM object (node, way, relation) in which the Mapillary picture was mentioned.

## Why did you write it?

I am an OpenStreetMap enthusiast who believes that the availablity of on-the-ground imagery provided by services such as Mapillary will lead to a quality-improvement of OSM data. My goal is to show Mapillary contributors that their images are used and thereby motivating them to keep on posting new ones.

## How does it work?

The bot downloads [augmented diffs](https://wiki.openstreetmap.org/wiki/Overpass_API/Augmented_Diffs) from the [Overpass API](https://wiki.openstreetmap.org/wiki/Overpass_API) every minute. Whenever the value of an arbitrary tag contains one or multiple links to mapillary images (or the tag `mapillary=` is used) the bot uses the [Mapillary API](https://a.mapillary.com) to post a comment to this image.

## How do I run it?

1. Create a text file `operator_email` with its first line being your email address. This email address will be sent to the Overpass API as part of the User-Agent string, so its operator can contact you if (s)he's not happy with your usage of the Overpass API.

2. Set up mapillary authentication. (TODO)

3. Run `mapillary-osm-usage-notification-bot.py`

## Developer information

### First time setup

```bash
    cd mapillary-osm-usage-notification-bot
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    deactivate
```

### Run tests

```bash
    cd mapillary-osm-usage-notification-bot
    source venv/bin/activate
    pytest
    deactivate
```

## How can I contact you?

I'd like to hear your feedback / get your pull requests. You can reach me by email: lukas.toggenburgerXXhtwchur.ch (replace XX with @)
