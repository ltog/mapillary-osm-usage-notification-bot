#!/usr/bin/python3

"""
Copyright 2016, 2017 Lukas Toggenburger; https://github.com/ltog/mapillary-osm-usage-notification-bot

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import argparse
import requests
import time # http://stackoverflow.com/a/64486
import sys # https://docs.python.org/3/library/sys.html#sys.exit
from lxml import etree
from collections import namedtuple
import re
import logging

operator_email = ''
mapillary_client_id = ''
mapillary_access_token = ''
user_agent = ''
successlog = ''
failurelog = ''
timeout = (9.05, 240)

Entry = namedtuple('Entry', 'osmtype osmid osmchangeset osmuid osmuser mapillaryid')


def main(args):
    global operator_email
    global mapillary_client_id
    global mapillary_access_token
    global user_agent
    operator_email = get_operator_email()
    mapillary_client_id = get_mapillary_client_id()
    mapillary_access_token = get_mapillary_access_token()
    user_agent = "mapillary-osm-usage-notification-bot operated by " + operator_email

    available = get_available() # the highest adiff id that is currently available

    first = init_first(args, available) # the first adiff id that will be processed
    last  = init_last(args)             # the last  adiff id that will be processed

    logging.debug("first = " + str(first))
    logging.debug("last = " + str(last))

    if (first > last):
        logging.critical("Invalid range for augmented diff IDs: first=" + str(first) + " last=" + str(last))
        exit(1)

    if (available < first): # first run was planned for the future
        time.sleep(60*(start-available))

    todo = first # the adiff id that is currently to be processed

    while todo <= last:
        logging.debug("todo = " + str(todo))

        available = update_available_until(available, todo)

        logging.debug("Going to download adiff " + str(todo))
        adiff = etree.fromstring(get_adiff(todo)) # download adiff
        #adiff = etree.parse("augmented_diff2_defekt.xml") # read local file (for debugging)

        entries = create_entries(adiff)
        if len(entries)==0:
            logging.info("adiff " + str(todo) + ": No Mapillary references found")
        else:
            logging.info("adiff " + str(todo) + ": Found the following references to Mapillary: " + str(entries))

        post_comments_to_mapillary(entries)

        if (todo+1 > available): # next adiff not available yet
            logging.debug("Next adiff is not available yet, going to sleep for just under a minute")
            time.sleep(58) # augmented diffs may theoretically appear in irregular intervals

        todo += 1


def update_available_until(available, todo):
    count = 0
    while available < todo:
        backoff_sleep(count)
        available = get_available()
        count += 1
    return available


def init_first(args, available):
    if args.first == None: # argument is not defined by parameter
        return available
    else:
        return args.first


def init_last(args):
    if args.last == None: # argument is not defined by parameter
        return sys.maxsize # http://stackoverflow.com/a/26121781
    else:
        return args.last


def create_entries(adiff):
    """
    Write a global list 'entries' containing namedtuples with attributes
    - osmtype: The OSM datatype: 'node', 'way' or 'relation'
    - osmid: The OSM ID of the object
    - mapillaryid: The Mapillary ID of the picture that was referenced
    """
    entries = set()

    # look at modified objects in their new state and add all
    # Mapillary references
    for tag in adiff.findall(".//action[@type='modify']/new//tag"):
        for e in get_entries(tag):
            entries.add(e)

    # Due to the structure of the augmented diff files (showing
    # the state before and after a changeset) we have to remove
    # these entries since the tags/references existed before.
    # This might remove a lot of the prevously added entries.
    for tag in adiff.findall(".//action[@type='modify']/old//tag"):
        for e in get_entries(tag):
            oldentries = set()
            oldentries.add(e)
            entries -= oldentries

    # also look at newly created objects and their tags
    for tag in adiff.findall(".//action[@type='create']//tag"):
        for e in get_entries(tag):
            entries.add(e)

    return entries

def get_operator_email():
    try:
        operator_email = open("operator_email", "r").readline().strip()
    except IOError as e:
        logging.critical("Plase provide a file 'operator_email' with the first line showing your email address. This email address will be sent to the Overpass API as part of the User-Agent string, so its operator can contact you if (s)he's not happy with your usage of the Overpass API. Exiting...")
        exit(1)

    if not re.match("^[^@]+@[^@]+\.[^@]+$", operator_email):
        logging.critical("The information provided in the file 'operator_email' doesn't look like a valid email address. Exiting...")
        exit(1)

    return operator_email


def get_mapillary_client_id():
    try:
        mapillary_client_id = open("mapillary_client_id", "r").readline().strip()
    except IOError as e:
        logging.critical("Please provide a file 'mapillary_client_id' containing the client_id from an application registered at Mapillary. Exiting...")
        exit(1)

    return mapillary_client_id


def get_mapillary_access_token():
    try:
        mapillary_access_token = open("mapillary_access_token", "r").readline().strip()
    except IOError as e:
        logging.critical("Please provide a file 'mapillary_access_token' containing the access_token from Mapillary. Exiting...")
        exit(1)

    return mapillary_access_token


def post_comments_to_mapillary(entries):
    global mapillary_client_id
    global mapillary_access_token
    global user_agent
    global time_out

    for e in entries:
        msg = "This image was mentioned on OpenStreetMap at https://www.openstreetmap.org/" + e.osmtype + "/" + e.osmid + " by user '" + e.osmuser + "'. Message generated by https://github.com/ltog/mapillary-osm-usage-notification-bot ."

        url = "https://a.mapillary.com/v2/im/" + e.mapillaryid + "/cm?client_id=" + mapillary_client_id
        payload = {"cm": msg}
        r = backoff_post(url, payload)
        logging.info("Posting comment for image " + e.mapillaryid + " resulted in HTTP status " + str(r.status_code) + " and response: " + r.text)
        if r.status_code == 200:
            log_success(str(e))
        if r.status_code == 404:
            logging.warning("Entry " + str(e) + "generated HTTP error 404 for URL " + url + " . ")
            log_failure(str(e))


def log_success(msg):
    global successlog
    log_to_file(msg, successlog)


def log_failure(msg):
    global failurelog
    log_to_file(msg, failurelog)


def log_to_file(msg, myfile):
    msg = msg.rstrip() + "\n" # ensure there's exactly one newline at the end
    path = ''
    if os.path.isabs(myfile): # http://stackoverflow.com/a/3320429
        path = myfile
    else:
        path =  os.getcwd() + "/" + myfile # http://stackoverflow.com/a/5137509
    with open(path, "a") as myfile:
        myfile.write(msg)


def get_entries(tag):
    """
    Return a set of Entries with information about Mapillary pictures contained in the given tag.
    """
    osmtype      = get_osmtype_from_tag(tag)
    osmid        = get_attribute_from_tag("id", tag)
    osmchangeset = get_attribute_from_tag("changeset", tag)
    osmuid       = get_attribute_from_tag("uid", tag)
    osmuser      = get_attribute_from_tag("user", tag)
    entries = set()

    for mapillaryid in get_mapillary_ids_from_tag(tag):
        entries.add(Entry(
            osmtype      = osmtype,
            osmid        = osmid,
            osmchangeset = osmchangeset,
            osmuid       = osmuid,
            osmuser      = osmuser,
            mapillaryid  = mapillaryid))

    return entries


def get_mapillary_ids_from_tag(tag):
    ids = get_mapillary_url_matches(tag.get("v"))

    if tag.get("k") == "mapillary": # (only) the tag 'mapillary' may contain plain mapillary ids that are not part of an url
        ids.extend(get_mapillary_key_matches(tag.get("v")))

    return ids


def get_osmtype_from_tag(tag):
    return tag.getparent().tag 


def get_attribute_from_tag(attribute, tag):
    """
    Return attribute value of the OSM object of this tag.

    If the tag is part of a modified object, always return attribute values of the object inside <new>, even if the tag was inside <old>.
    """
    if is_action_create(tag):
        return tag.getparent().get(attribute)
    else:
        return tag.getparent().getparent().getparent().find("new")[0].get(attribute)


def is_action_create(tag):
    """
    Is this tag part of a newly created OSM object or a modified one?
    """
    if tag.getparent().getparent().get("type") == "create":
        return True
    else:
        return False


def get_mapillary_url_matches(mystring):
    matches = re.findall(r"https?://(?:www[.])?mapillary[.]com/map/im/([a-zA-Z0-9_-]{22}).*?|https?://(?:www[.])?mapillary[.]com/app.*?[?].*?&pKey=([a-zA-Z0-9_-]{22}).*?", mystring)

    return sanitize_findall_output(matches)


def sanitize_findall_output(matches):
    """
    re.findall(), when working on a regex containing groups in alternatives, returns a list of tuples, whereas in each tuple only one value is the content matched in the group. The other values of the tuples are empty strings. This function converts such a list of tuples with empty strings to a proper list of string without empty strings.
    """
    out = []

    for match in matches:
        for group in match:
            if group != '':
                out.append(group)
                continue

    return out


def get_mapillary_key_matches(mystring):
    return re.findall(r"([a-zA-Z0-9_-]{22})", mystring)


def get_available():
    """
    Download the ID of the highest available augmented diff.
    """
    logging.debug("Going to download the ID of the highest available augmented diff")
    available = int(backoff_get("http://overpass-api.de/api/augmented_diff_status").text)
    logging.debug("Highest available Overpass ID = " + str(available))
    return available


def get_adiff(overpass_id):
    return backoff_get("http://overpass-api.de/api/augmented_diff?id=" + str(overpass_id)).content


def backoff_sleep(count):
    duration = 10*(2**count-1) # 0, 10, 30, 70, 150, 310, ...

    if (duration > 0):
        logging.debug("Duration of backoff sleep = " + str(duration))

    time.sleep(duration)

def backoff_get(url):
    return generic_backoff_download(url, "GET", None)

def backoff_post(url, payload):
    return generic_backoff_download(url, "POST", payload)

def generic_backoff_download(url, method, payload):
    global user_agent
    global timeout
    global mapillary_access_token

    count = 0
    while True:
        backoff_sleep(count)
        count += 1

        r = None
        headers={'User-Agent': user_agent}

        try:
            if (method.upper() == "GET"):
                r = requests.get(url, timeout=timeout, headers=headers)
            elif (method.upper() == "POST"):
                headers.update({"Authorization": "Bearer " + mapillary_access_token})
                r = requests.post(url, timeout=timeout, headers=headers, data=payload)
        except requests.exceptions.RequestException as e: # http://docs.python-requests.org/en/latest/user/quickstart/#errors-and-exceptions , e.g. 404 throws no exception
            logging.warning("Got exception " + str(e) + " for URL " + url + " , but will continue trying to fetch it.")
            continue

        r.encoding = 'utf-8' # if encoding is not set, python will spend quite some cpu time trying to determine the encoding when using r.text, see also https://github.com/kennethreitz/requests/issues/1604#issuecomment-24476927

        if r.status_code == 200:
            return r
        elif r.status_code == 404: # 404 generates no exception
            return r


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Read OSM augmented diffs and report mentioned Mapillary pictures back to Mapillary in form of image comments.')

    parser.add_argument("--first", type=int, help="Overpass ID of the first augmented diff to process (defaults to the highest ID currently available", required=False)
    parser.add_argument("--last", type=int, help="Overpass ID of the last augmented diff to process (defaults to a large value, practically equivalent to never)", required=False)
    # TODO: allow first, last to be given as iso8601 timestamp
    parser.add_argument("--loglevel", type=str, help="Override the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL) (default: %(default)s)", required=False, default="INFO") # http://stackoverflow.com/a/18507871
    parser.add_argument("--successlog", type=str, help="Logfile for successfully posted comments (default: %(default)s)", required=False, default="success.log")
    parser.add_argument("--failurelog", type=str, help="Logfile for unsuccessfully posted comments (default: %(default)s)", required=False, default="failure.log")

    args = parser.parse_args()

    numeric_log_level = getattr(logging, args.loglevel.upper(), None) # https://docs.python.org/3.5/howto/logging.html#logging-to-a-file
    if not isinstance(numeric_log_level, int):
        logging.critical("Invalid log level. Exiting...")
        exit(1)

    successlog = args.successlog
    failurelog = args.failurelog

    logging.basicConfig(level=numeric_log_level, format="%(asctime)s %(levelname)s %(message)s") # https://docs.python.org/3.5/howto/logging.html
    logging.getLogger("requests").setLevel(logging.WARNING) # disable messages from module "requests", unless it's at least a warning, see http://stackoverflow.com/a/11029841
    logging.getLogger("urllib3").setLevel(logging.WARNING) # disable messages from module "urllib3" (used by "requests"), unless it's at least a warning, see http://stackoverflow.com/a/11029841

    main(args)

